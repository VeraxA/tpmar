requirejs(['ModulesLoaderV2.js'], function()
		{ 
			ModulesLoader.requireModules(["threejs/three.min.js"]) ;
			ModulesLoader.requireModules([ "myJS/ThreeRenderingEnv.js"]);
		}
) ;
Helico = function(){
	console.log("Test2");
	var Loader = new ThreeLoadingEnv();
	var helico = new THREE.Object3D(); 
	var helicoCorp = Loader.load({filename: 'assets/helico/helicoCorp.obj', node: helico, name: 'helicoCorp'}) ;
	var axeCentre = Loader.load({filename: 'assets/helico/axe.obj', node: helico, name: 'axeCentre'}) ;
	//var pale1 = Loader.load({filename: 'assets/helico/pale.obj', node: helico, name: 'pale1'}) ;
	var axeDroit = Loader.load({filename: 'assets/helico/axe.obj', node: helico, name: 'axeDroit'}) ;
	var axeGauche = Loader.load({filename: 'assets/helico/axe.obj', node: helico, name: 'axeGauche'}) ;
	var turbineCentre = Loader.load({filename: 'assets/helico/turbine.obj', node: helico, name: 'turbineCentre'}) ;
	var turbineDroit = Loader.load({filename: 'assets/helico/turbine.obj', node: helico, name: 'turbineDroit'}) ;
	var turbineGauche = Loader.load({filename: 'assets/helico/turbine.obj', node: helico, name: 'turbineGauche'}) ;
	var paleC1 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeCentre, name: 'paleC1'}) ;
	var paleC2 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeCentre, name: 'paleC2'}) ;
	var paleC3 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeCentre, name: 'paleC3'}) ;
	var paleD1 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeDroit, name: 'paleD1'}) ;
	var paleD2 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeDroit, name: 'paleD2'}) ;
	var paleD3 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeDroit, name: 'paleD3'}) ;
	var paleG1 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeGauche, name: 'paleG1'}) ;
	var paleG2 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeGauche, name: 'paleG2'}) ;
	var paleG3 = Loader.load({filename: 'assets/helico/pale2.obj', node: axeGauche, name: 'paleG3'}) ;
	paleC1.position.y = 2;
	paleC2.position.y = 2;
	paleC3.position.y = 2;
	paleC2.rotation.y = 3.1415*2/3;
	paleC3.rotation.y = 3.1415*4/3;
	paleD1.position.y = 2;
	paleD2.position.y = 2;
	paleD3.position.y = 2;
	paleD2.rotation.y = 3.1415*2/3;
	paleD3.rotation.y = 3.1415*4/3;
	paleG1.position.y = 2;
	paleG2.position.y = 2;
	paleG3.position.y = 2;
	paleG2.rotation.y = 3.1415*2/3;
	paleG3.rotation.y = 3.1415*4/3;
	//paleD1.position
	//axeCentre.rotation.x = 3.1415/2;
	axeCentre.position.y = 1;
	//axeDroit.position.x = 8.5;
	axeDroit.position.y = 1;
	//axeDroit.position.z = 4;
	//axeGauche.position.x = -8.5;
	axeGauche.position.y = 1;
	//axeGauche.position.z = 4;
	turbineCentre.position.z = 4;
	turbineCentre.rotation.x = 3.1415/2;
	turbineDroit.position.x = 8.5;
	turbineDroit.position.y = -3;
	turbineDroit.position.z = 4;
	turbineGauche.position.x = -8.5;
	turbineGauche.position.y = -3;
	turbineGauche.position.z = 4;
	//var cameraGlobalView = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.01, 5000 );
		//axe.add(cameraGlobalView) ;
	axeCentre.add(paleC1);
	axeCentre.add(paleC2);
	axeCentre.add(paleC3);

	axeDroit.add(paleD1);
	axeDroit.add(paleD2);
	axeDroit.add(paleD3);

	axeGauche.add(paleG1);
	axeGauche.add(paleG2);
	axeGauche.add(paleG3);

	turbineGauche.add(axeGauche);
	turbineDroit.add(axeDroit);
	turbineCentre.add(axeCentre);

	/*helicoCorp.add(axeCentre);	
	helicoCorp.add(axeDroit);
	helicoCorp.add(axeGauche);*/
	helicoCorp.add(turbineCentre);
	helicoCorp.add(turbineDroit);
	helicoCorp.add(turbineGauche);
	helico.add(helicoCorp);

	return helico

}