/**
 *  ThreeJS test file using the ThreeRender class
 */

//Loads all dependencies
requirejs(['ModulesLoaderV2.js'], function()
		{ 
			// Level 0 includes
			ModulesLoader.requireModules(["threejs/three.min.js"]) ;
			ModulesLoader.requireModules([ "myJS/ThreeRenderingEnv.js", 
			                              "myJS/ThreeLightingEnv.js", 
						      "http://mrdoob.github.io/three.js/examples/fonts/helvetiker_regular.typeface.js",
						      "http://mrdoob.github.io/three.js/examples/fonts/helvetiker_bold.typeface.js",
			                              "myJS/ThreeLoadingEnv.js", 
			                              "myJS/navZ.js",
			                              "FlyingVehicle.js",
			                              "myJS/helico.js"]) ;
			// Loads modules contained in includes and starts main function
			ModulesLoader.loadModules(start) ;
		}
) ;

function start(){
	//	----------------------------------------------------------------------------
	//	MAR 2014 - nav test
	//	author(s) : Cozot, R. and Lamarche, F.
	//	date : 11/16/2014
	//	last : 11/25/2014
	//	---------------------------------------------------------------------------- 			
	//	global vars
	//	----------------------------------------------------------------------------
	//	keyPressed
	var currentlyPressedKeys = {};
	
	// car Position
	var CARx = -220; 
	var CARy = 0 ; 
	var CARz = 0 ;
	var CARtheta = 0 ; 
	// car speed
	var dt = 0.05; 
	var dx = 1.0;

	// Creates the vehicle (handled by physics)
	var vehicle = new FlyingVehicle(
			{
				position: new THREE.Vector3(CARx, CARy, CARz),
				zAngle : CARtheta+Math.PI/2.0,
			}
			) ;

	var phantom = new FlyingVehicle(
			{
				position: new THREE.Vector3(CARx, CARy, CARz),
				zAngle : CARtheta+Math.PI/2.0,
			}
			) ;
	
	//	rendering env
	var RC =  new ThreeRenderingEnv();

	//	lighting env
	var Lights = new ThreeLightingEnv('rembrandt','neutral','spot',RC,5000);

	//	Loading env
	var Loader = new ThreeLoadingEnv();

	//	Meshes
	Loader.loadMesh('assets','border_Zup_02','obj',	RC.scene,'border',	-340,-340,0,'front');
	Loader.loadMesh('assets','ground_Zup_03','obj',	RC.scene,'ground',	-340,-340,0,'front');
	Loader.loadMesh('assets','circuit_Zup_02','obj',RC.scene,'circuit',	-340,-340,0,'front');
	//Loader.loadMesh('assets','tree_Zup_02','obj',	RC.scene,'trees',	-340,-340,0,'double');
	Loader.loadMesh('assets','arrivee_Zup_01','obj',	RC.scene,'decors',	-340,-340,0,'front');
		
	//	Car
	// car Translation
	var car0 = new THREE.Object3D(); 
	car0.name = 'car0'; 
	RC.addToScene(car0); 
	// initial POS
	car0.position.x = CARx;
	car0.position.y = CARy;
	car0.position.z = CARz;
	// car Rotation floor slope follow
	var car1 = new THREE.Object3D(); 
	car1.name = 'car1';
	car0.add(car1);
	// car vertical rotation
	var car2 = new THREE.Object3D(); 
	car2.name = 'car2';
	car1.add(car2);
	car2.rotation.z = CARtheta ;
	// the car itself 
	// simple method to load an object
	//var car3 = Loader.load({filename: 'assets/car_Zup_01.obj', node: car2, name: 'car3'}) ;
	var car3 = new Helico();
	car2.add(car3);
	car3.position.z= +0.25 ;
	// attach the scene camera to car
	//car1.add(RC.camera) ;
	//RC.camera.position.x = 0.0 ;
	//RC.camera.position.z = 10.0 ;
	//RC.camera.position.y = 100;
	//RC.camera.rotation.x = 180 ;
	//RC.camera.rotation= -90;
	var currentCamera = 0; 	// 0 : camera in car, 1 : on circuit, 2 very hight

	var nbTourMax = 2;	// number of laps

	var text = "Lap 1/" + nbTourMax;	// initial text
	var tempMeshText;
	var tempMeshTime;
	var tempMeshCheckpoints;
	var tempMeshCheckpointsTime;
	var tempMeshTime1;
	var tempMeshFinalEnd;
	var tempMeshFinalTime;
	var checkZone1 = 0;			// for time at checkpoints
	var checkZone2 = 0;
	var checkZone3 = 0;
	var colorLaps = new THREE.Color("rgb(228,1,1)");
	var colorTime = new THREE.Color("rgb(255,255,255)");

	var clock = new THREE.Clock();
	clock.start();

	var totalTime = 0;

	createText();

	var nbTourReplay = 0;

	var zone7 = false;
	var zone14 = false;
	var zone27 = false;
	var tour = 1;

	var over = false;
	var Replay = false;		// running
	var replayOver = false;		// replay over
	var beatThePhantom = false;	// challenge our phantom

	/* Phantom */
	var car0_phan;
	var car1_phan;
	var car2_phan;
	var car3_phan;
	var NAV_phan;

	var startedPhantomMode = false;

	var indexPosition = 0;
	var indexRotation = 0;

	var recordPositions = [];
	var recordRotations = [];

	//	Skybox
	Loader.loadSkyBox('assets/maps',['px','nx','py','ny','pz','nz'],'jpg', RC.scene, 'sky',4000);

	//	Planes Set for Navigation 
	// 	z up 
	var NAV = new navPlaneSet(
					new navPlane('p01',	-260, -180,	 -80, 120,	+0,+0,'px')); 		// 01	
	NAV.addPlane(	new navPlane('p02', -260, -180,	 120, 200,	+0,+20,'py')); 		// 02		
	NAV.addPlane(	new navPlane('p03', -260, -240,	 200, 240,	+20,+20,'px')); 	// 03		
	NAV.addPlane(	new navPlane('p04', -240, -160,  200, 260,	+20,+20,'px')); 	// 04		
	NAV.addPlane(	new navPlane('p05', -160,  -80,  200, 260,	+20,+40,'px')); 	// 05		
	NAV.addPlane(	new navPlane('p06',  -80, -20,   200, 260,	+40,+60,'px')); 	// 06		
	NAV.addPlane(	new navPlane('p07',  -20,  +40,  140, 260,	+60,+60,'px')); 	// 07		
	NAV.addPlane(	new navPlane('p08',    0,  +80,  100, 140,	+60,+60,'px')); 	// 08		
	NAV.addPlane(	new navPlane('p09',   20, +100,   60, 100,	+60,+60,'px')); 	// 09		
	NAV.addPlane(	new navPlane('p10',   40, +100,   40,  60,	+60,+60,'px')); 	// 10		
	NAV.addPlane(	new navPlane('p11',  100,  180,   40, 100,	+40,+60,'nx')); 	// 11		
	NAV.addPlane(	new navPlane('p12',  180,  240,   40,  80,	+40,+40,'px')); 	// 12		
	NAV.addPlane(	new navPlane('p13',  180,  240,    0,  40,	+20,+40,'py')); 	// 13 		
	NAV.addPlane(	new navPlane('p14',  200,  260,  -80,   0,	+0,+20,'py')); 		// 14		
	NAV.addPlane(	new navPlane('p15',  180,  240, -160, -80,	+0,+40,'ny')); 		// 15		
	NAV.addPlane(	new navPlane('p16',  160,  220, -220,-160,	+40,+40,'px')); 	// 16	
	NAV.addPlane(	new navPlane('p17',   80,  160, -240,-180,	+40,+40,'px')); 	// 17	
	NAV.addPlane(	new navPlane('p18',   20,   80, -220,-180,	+40,+40,'px')); 	// 18	
	NAV.addPlane(	new navPlane('p19',   20,   80, -180,-140,	+40,+60,'py')); 	// 19	
	NAV.addPlane(	new navPlane('p20',   20,   80, -140,-100,	+60,+80,'py')); 	// 20	
	NAV.addPlane(	new navPlane('p21',   20,   60, -100, -40,	+80,+80,'px')); 	// 21		
	NAV.addPlane(	new navPlane('p22',  -80,   20, -100, -40,	+80,+80,'px')); 	// 22		
	NAV.addPlane(	new navPlane('p23', -140,  -80, -100, -40,	+80,+80,'px')); 	// 23		
	NAV.addPlane(	new navPlane('p24', -140,  -80, -140,-100,	+60,+80,'py')); 	// 24		
	NAV.addPlane(	new navPlane('p25', -140,  -80, -200,-140,	+40,+60,'py')); 	// 25		
	NAV.addPlane(	new navPlane('p26', -100,  -80, -240,-200,	+40,+40,'px')); 	// 26		
	NAV.addPlane(	new navPlane('p27', -220, -100, -260,-200,	+40,+40,'px')); 	// 27	
	NAV.addPlane(	new navPlane('p28', -240, -220, -240,-200,	+40,+40,'px')); 	// 28	
	NAV.addPlane(	new navPlane('p29', -240, -180, -200,-140,	+20,+40,'ny')); 	// 29	
	NAV.addPlane(	new navPlane('p30', -240, -180, -140, -80,	+0,+20,'ny')); 		// 30			
	NAV.setPos(CARx,CARy,CARz); 
	NAV.initActive();
	// DEBUG
	//NAV.debug();
	//var navMesh = NAV.toMesh();
	//RC.addToScene(navMesh);
	//	event listener
	//	---------------------------------------------------------------------------
	//	resize window
	window.addEventListener( 'resize', onWindowResize, false );
	//	keyboard callbacks 
	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;					

	//	callback functions
	//	---------------------------------------------------------------------------
	function handleKeyDown(event) { currentlyPressedKeys[event.keyCode] = true;}
	function handleKeyUp(event) {currentlyPressedKeys[event.keyCode] = false;}

	/* attach the text to the view to display laps */
	function createText() {
		if(tour > 0) { deleteText(); }
		var textGeom = new THREE.TextGeometry(text, 
		{
			size: 3, height: 0,
			font: "helvetiker", weight: "bold", style: "normal"
		});
		var material = new THREE.MeshPhongMaterial({
			color: colorLaps
	   	});
		var textMesh = new THREE.Mesh( textGeom, material );
		tempMeshText = textMesh;
		textMesh.position.x = -42;
		textMesh.position.y = 5;
		textMesh.position.z = 29;
		textMesh.rotation.x = Math.PI / 2;
		car3.add(textMesh);

		textGeom.computeBoundingBox();
	    	textGeom.textWidth = textGeom.boundingBox.max.x - textGeom.boundingBox.min.x;
	}

	/* update number of laps (delete) */
	function deleteText() {
		car3.remove(tempMeshText);
	}

	/* attach the text to the view to display time of the last lap */
	function createTime() {
		deleteTime();
		var textGeom = new THREE.TextGeometry( "Last lap ", 
		{
			size: 3, height: 0,
			font: "helvetiker", weight: "bold", style: "normal"
		});
		var material = new THREE.MeshPhongMaterial({
			color: colorLaps
	   	});
		var textMesh = new THREE.Mesh( textGeom, material );
		tempMeshTime = textMesh;
		textMesh.position.x = -10;
		textMesh.position.y = 5;
		textMesh.position.z = 29;
		textMesh.rotation.x = Math.PI / 2;
		car3.add(textMesh);

		var time = Math.round(clock.getElapsedTime()*1000)/1000;
		var textGeom1 = new THREE.TextGeometry(time + " s", 
		{
			size: 3, height: 0,
			font: "helvetiker", weight: "bold", style: "normal"
		});
		totalTime = totalTime + time;
		var material1 = new THREE.MeshPhongMaterial({
			color: colorLaps
	   	});
		var textMesh1 = new THREE.Mesh( textGeom1, material1 );
		tempMeshTime1 = textMesh1;
		textMesh1.position.x = -11;
		textMesh1.position.y = 15;
		textMesh1.position.z = 29;
		textMesh1.rotation.x = Math.PI / 2;
		car3.add(textMesh1);

		textGeom1.computeBoundingBox();
	    	textGeom1.textWidth = textGeom1.boundingBox.max.x - textGeom1.boundingBox.min.x;
	}

	/* update time (delete) */
	function deleteTime() {
		car3.remove(tempMeshTime);
		car3.remove(tempMeshTime1);
	}

	/* attach the text to the view to display time at the checkpoints */
	function createTimeCheckpoints(position) {
		deleteTimeCheckpoints();
		var textGeom0 = new THREE.TextGeometry("Checkpoint",
		{
			size: 3, height: 0,
			font: "helvetiker", weight: "bold", style: "normal"
		});
		var material0 = new THREE.MeshPhongMaterial({
			color: colorLaps
	   	});
		var textMesh0 = new THREE.Mesh( textGeom0, material0 );
		tempMeshCheckpoints = textMesh0;
		textMesh0.position.x = 19;
		textMesh0.position.y = 5;
		textMesh0.position.z = 29;
		textMesh0.rotation.x = Math.PI / 2;
		car3.add(textMesh0);

		var textGeom = new THREE.TextGeometry((Math.round(clock.getElapsedTime()*1000)/1000) + " s",
		{
			size: 3, height: 0,
			font: "helvetiker", weight: "bold", style: "normal"
		});
		var material = new THREE.MeshPhongMaterial({
			color: colorLaps
	   	});
		var textMesh = new THREE.Mesh( textGeom, material );
		tempMeshCheckpointsTime = textMesh;
		textMesh.position.x = 34;
		textMesh.position.y = 15;
		textMesh.position.z = 29;
		textMesh.rotation.x = Math.PI / 2;
		car3.add(textMesh);

		textGeom.computeBoundingBox();
	    	textGeom.textWidth = textGeom.boundingBox.max.x - textGeom.boundingBox.min.x;
		if(position == 1) { checkZone1 = 1; }
		else if(position == 2) { checkZone2 = 1; }
		else if(position == 3) { checkZone3 = 1; }
	}

	/* update time (delete) */
	function deleteTimeCheckpoints() {
		car3.remove(tempMeshCheckpoints);
		car3.remove(tempMeshCheckpointsTime);
	}

	function endRace() {
		/*var textGeom1 = new THREE.TextGeometry("END",
		{
			size: 7, height: 0,
			font: "helvetiker", weight: "bold", style: "normal"
		});
		var material1 = new THREE.MeshPhongMaterial({
			color: colorLaps
	   	});
		var textMesh1 = new THREE.Mesh( textGeom1, material1 );
		tempMeshFinalEnd = textMesh1;
		textMesh1.position.x = -5;
		textMesh1.position.y = 20;
		textMesh1.position.z = 29;
		textMesh1.rotation.x = Math.PI / 2;

		car3.add(textMesh1);*/

		var textGeom2 = new THREE.TextGeometry("Final time : " + totalTime + " s",
		{
			size: 5, height: 0,
			font: "helvetiker", weight: "bold", style: "normal"
		});
		var material2 = new THREE.MeshPhongMaterial({
			color: colorLaps
	   	});
		var textMesh2 = new THREE.Mesh( textGeom2, material2 );
		tempMeshFinalTime = textMesh2;
		textMesh2.position.x = -26;
		textMesh2.position.y = 10;
		textMesh2.position.z = 15;
		textMesh2.rotation.x = Math.PI / 2;

		car3.add(textMesh2);
	}

	function deleteTextEndRace() {
		//car3.remove(tempMeshFinalEnd);
		car3.remove(tempMeshFinalTime);
	}

	function PositionChange(newPosition){
		if(currentCamera==1){
			if((NAV.findActive(newPosition.x,newPosition.y)>=6)&&(NAV.findActive(newPosition.x,newPosition.y)<8)){
				RC.camera.position.set(-130,250,120);
			}
			if((NAV.findActive(newPosition.x,newPosition.y)>=8)&&(NAV.findActive(newPosition.x,newPosition.y)<11)){
				RC.camera.position.set(0,100,90);
			}
			if((NAV.findActive(newPosition.x,newPosition.y)>=10)&&(NAV.findActive(newPosition.x,newPosition.y)<13)){
				RC.camera.position.set(255,140,80);
				RC.camera.rotation.y = 120;
			}
			if((NAV.findActive(newPosition.x,newPosition.y)>=13)&&(NAV.findActive(newPosition.x,newPosition.y)<15)){
				RC.camera.position.set(310,-75,30);
				RC.camera.rotation.y = 90;
			}
			if((NAV.findActive(newPosition.x,newPosition.y)>=15)&&(NAV.findActive(newPosition.x,newPosition.y)<22)){
				RC.camera.position.set(100,-333,100);
				RC.camera.rotation.y = 60;
			}
			if((NAV.findActive(newPosition.x,newPosition.y)>=22)&&(NAV.findActive(newPosition.x,newPosition.y)<30)){
				RC.camera.position.set(-200,-350,100);
				RC.camera.rotation.y = 60;
			}
		}
		verifPrec(newPosition);

	}

	function verifPrec(newPosition){
		if(((NAV.findActive(newPosition.x,newPosition.y)==1)&&zone7&&zone14&&zone27)) {
			if(tour < nbTourMax+1 && !(beatThePhantom) && !(Replay)) {
				tour++;
				text = "Lap " + tour + "/" + nbTourMax;
				createText();
				zone7=false;
				zone14=false;
				zone27=false;
				checkZone1 = 0;
				checkZone2 = 0;
				checkZone3 = 0;
				createTime();
				clock.stop();
				clock = new THREE.Clock();
				clock.start();
				deleteTimeCheckpoints();
			}

			if(!(Replay) && tour == nbTourMax+1) {			// end of the race
				console.log("mode replay");
				endRace();
				deleteText();
				deleteTime();
				deleteTimeCheckpoints();
				over = true;
				tour = 0;

				NAV.setPos(CARx,CARy,CARz); 		// reset position for the phantom
				NAV.initActive();
				
				currentCamera = 1;
				indexPosition = 0;
				indexRotation = 0;
				nbTourReplay = 0;
			}

			else if((Replay == true) && nbTourReplay < nbTourMax) { 
				nbTourReplay++;
			}
		
			/*if(beatThePhantom == true) {			// mode "beat the phantom"
				console.log("mode beat the phantom");
				tour++;
				text = "Lap " + tour + "/" + nbTourMax;
				createText();
				zone7=false;
				zone14=false;
				zone27=false;
				checkZone1 = 0;
				checkZone2 = 0;
				checkZone3 = 0;
				createTime();
				clock.stop();
				clock = new THREE.Clock();
				clock.start();
				deleteTimeCheckpoints();
				deleteTextEndRace();
				over = false;
				replayOver = false;

				NAV.setPos(CARx,CARy,CARz); // reset position for the phantom
				NAV.initActive();
				
				currentCamera = 0;
				indexPosition = 0;
				indexRotation = 0;
				nbTourReplay = 0;
			}*/

			/*else if (tour == nbTourMax && tourReplay == nbTourMax) {
				beatPhantom = true;
				endRace();
				deleteText();
				deleteTime();
				deleteTimeCheckpoints();
				deleteTextEndRace();
				over = true;

				NAV.setPos(CARx,CARy,CARz); // reset position for the phantom
				NAV.initActive();
				
				currentCamera = 0;
				indexPosition = 0;
				indexRotation = 0;
			}*/
		}
		else{
			if((NAV.findActive(newPosition.x,newPosition.y)==6)&&!(zone14)&&!(zone27)){
				zone7=true;
				if((checkZone1 == 0)&&!(over)) { createTimeCheckpoints(1); }	// display time at the first checkpoint
			}
			if((NAV.findActive(newPosition.x,newPosition.y)==13)&&(zone7)&&!(zone27)){
				zone14=true;
			}
			if((NAV.findActive(newPosition.x,newPosition.y)==15)&&(zone7)&&(zone14)&&!(zone27)){
				zone14=true;
				if((checkZone2 == 0)&&!(over)) { createTimeCheckpoints(2); }	// display time at the second checkpoint
			}
			if((NAV.findActive(newPosition.x,newPosition.y)==26)&&(zone7)&&(zone14)){
				zone27=true;
				if((checkZone3 == 0)&&!(over)) { createTimeCheckpoints(3); }	// display time at the third checkpoint
			}
		}
	}

	function handleKeys() {
		if (currentlyPressedKeys[67]) // (C) debug
		{
			// debug scene
			RC.scene.traverse(function(o){
				console.log('object:'+o.name+'>'+o.id+'::'+o.type);
			});
		}				
		if (currentlyPressedKeys[68]) // (D) Right
		{
			vehicle.turnRight(1000) ;
		}
		if (currentlyPressedKeys[81]) // (Q) Left 
		{		
			vehicle.turnLeft(1000) ;
		}
		if (currentlyPressedKeys[90]) // (Z) Up
		{
			vehicle.goFront(1200, 1200) ;
		}
		if (currentlyPressedKeys[83]) // (S) Down 
		{
			vehicle.brake(700) ;
		}
		if (currentlyPressedKeys[84]) // (T) Camera change 
		{
			currentCamera++;
			if(currentCamera==3){
				currentCamera=0;
			}
		}
	}

	function replay(IndPosition, IndRotation) {
		Replay = true;
		phantom.stabilizeSkid(50) ; 
		phantom.stabilizeTurn(1000) ;

		phantom.update(1.0/60) ;
		// NAV
		NAV.move(recordPositions[IndPosition].x, recordPositions[IndPosition].y, 150,10) ;
		// car0
		car0.position.set(NAV.x, NAV.y, NAV.z) ;
		// Updates the phantom
		phantom.position.x = NAV.x ;
		phantom.position.y = NAV.Y ;

		car2.rotation.z = recordRotations[IndRotation];
	}

	function replayPhantom(IndPosition, IndRotation) {
		phantom.stabilizeSkid(50) ; 
		phantom.stabilizeTurn(1000) ;

		phantom.update(1.0/60) ;
		// NAV
		NAV_phan.move(recordPositions[IndPosition].x, recordPositions[IndPosition].y, 150,10) ;
		// car0
		car0_phan.position.set(NAV_phan.x, NAV_phan.y, NAV_phan.z) ;
		// Updates the phantom
		phantom.position.x = NAV_phan.x ;
		phantom.position.y = NAV_phan.Y ;

		car2_phan.rotation.z = recordRotations[IndRotation];
	}

	function initPhantomMode() {
		car0_phan = new THREE.Object3D(); 
		car0_phan.name = 'car0_phan'; 
		RC.addToScene(car0_phan); 
		// initial POS
		car0_phan.position.x = CARx;
		car0_phan.position.y = CARy;
		car0_phan.position.z = CARz;
		// car Rotation floor slope follow
		car1_phan = new THREE.Object3D(); 
		car1_phan.name = 'car1_phan';
		car0_phan.add(car1_phan);
		// car vertical rotation
		car2_phan = new THREE.Object3D(); 
		car2_phan.name = 'car2_phan';
		car1_phan.add(car2_phan);
		car2_phan.rotation.z = CARtheta ;
		// the car itself 
		// simple method to load an object
		car3_phan = Loader.load({filename: 'assets/car_Zup_01.obj', node: car2_phan, name: 'car3_phan'}) ;
		car3_phan.position.z= +0.25 ;

		NAV_phan = new navPlaneSet(
					new navPlane('p01', -260, -180,	 -80, 120,	+0,+0,'px')); 		// 01	
		NAV_phan.addPlane(	new navPlane('p02', -260, -180,	 120, 200,	+0,+20,'py')); 		// 02		
		NAV_phan.addPlane(	new navPlane('p03', -260, -240,	 200, 240,	+20,+20,'px')); 	// 03		
		NAV_phan.addPlane(	new navPlane('p04', -240, -160,  200, 260,	+20,+20,'px')); 	// 04		
		NAV_phan.addPlane(	new navPlane('p05', -160,  -80,  200, 260,	+20,+40,'px')); 	// 05		
		NAV_phan.addPlane(	new navPlane('p06',  -80, -20,   200, 260,	+40,+60,'px')); 	// 06		
		NAV_phan.addPlane(	new navPlane('p07',  -20,  +40,  140, 260,	+60,+60,'px')); 	// 07		
		NAV_phan.addPlane(	new navPlane('p08',    0,  +80,  100, 140,	+60,+60,'px')); 	// 08		
		NAV_phan.addPlane(	new navPlane('p09',   20, +100,   60, 100,	+60,+60,'px')); 	// 09		
		NAV_phan.addPlane(	new navPlane('p10',   40, +100,   40,  60,	+60,+60,'px')); 	// 10		
		NAV_phan.addPlane(	new navPlane('p11',  100,  180,   40, 100,	+40,+60,'nx')); 	// 11		
		NAV_phan.addPlane(	new navPlane('p12',  180,  240,   40,  80,	+40,+40,'px')); 	// 12		
		NAV_phan.addPlane(	new navPlane('p13',  180,  240,    0,  40,	+20,+40,'py')); 	// 13 		
		NAV_phan.addPlane(	new navPlane('p14',  200,  260,  -80,   0,	+0,+20,'py')); 		// 14		
		NAV_phan.addPlane(	new navPlane('p15',  180,  240, -160, -80,	+0,+40,'ny')); 		// 15		
		NAV_phan.addPlane(	new navPlane('p16',  160,  220, -220,-160,	+40,+40,'px')); 	// 16	
		NAV_phan.addPlane(	new navPlane('p17',   80,  160, -240,-180,	+40,+40,'px')); 	// 17	
		NAV_phan.addPlane(	new navPlane('p18',   20,   80, -220,-180,	+40,+40,'px')); 	// 18	
		NAV_phan.addPlane(	new navPlane('p19',   20,   80, -180,-140,	+40,+60,'py')); 	// 19	
		NAV_phan.addPlane(	new navPlane('p20',   20,   80, -140,-100,	+60,+80,'py')); 	// 20	
		NAV_phan.addPlane(	new navPlane('p21',   20,   60, -100, -40,	+80,+80,'px')); 	// 21		
		NAV_phan.addPlane(	new navPlane('p22',  -80,   20, -100, -40,	+80,+80,'px')); 	// 22		
		NAV_phan.addPlane(	new navPlane('p23', -140,  -80, -100, -40,	+80,+80,'px')); 	// 23		
		NAV_phan.addPlane(	new navPlane('p24', -140,  -80, -140,-100,	+60,+80,'py')); 	// 24		
		NAV_phan.addPlane(	new navPlane('p25', -140,  -80, -200,-140,	+40,+60,'py')); 	// 25		
		NAV_phan.addPlane(	new navPlane('p26', -100,  -80, -240,-200,	+40,+40,'px')); 	// 26		
		NAV_phan.addPlane(	new navPlane('p27', -220, -100, -260,-200,	+40,+40,'px')); 	// 27	
		NAV_phan.addPlane(	new navPlane('p28', -240, -220, -240,-200,	+40,+40,'px')); 	// 28	
		NAV_phan.addPlane(	new navPlane('p29', -240, -180, -200,-140,	+20,+40,'ny')); 	// 29	
		NAV_phan.addPlane(	new navPlane('p30', -240, -180, -140, -80,	+0,+20,'ny')); 		// 30			
		NAV_phan.setPos(CARx,CARy,CARz); 
		NAV_phan.initActive();

		startedPhantomMode = true;
	}

	//	window resize
	function  onWindowResize() {RC.onWindowResize(window.innerWidth,window.innerHeight);}

	function render() { 
			var angleOrient = Math.atan2(vehicle.getSpeed().x,vehicle.getSpeed().y);
			var angleAct = Math.atan2(car3.rotation.y,car3.rotation.x);
			var other = angleAct*angleOrient;
			//console.log("Angle :"+other);
			var rotationPale = Math.sqrt(Math.pow(vehicle.getSpeed().x,2)+Math.pow(vehicle.getSpeed().y,2));
			//console.log("R : "+rotationPale);
			if(rotationPale > 200)
				roationPale = 200;
			car3.children[0].children[0].rotation.y += rotationPale/250;
			car3.children[0].children[1].rotation.y += rotationPale/250;
			car3.children[0].children[2].rotation.y += rotationPale/250;
			car3.rotation.z = other;
			//console.log("X : " + vehicle.getSpeed().x);
		//if(!(beatThePhantom)) {
			//Camera in circuit
			RC.camera.position.set(-250,250,30);//Camera 1
			RC.camera.up = new THREE.Vector3(0,0,1);
			RC.camera.lookAt(car0.position);

			//camera in car
			var cameraGlobalView = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.01, 5000 );
			car3.add(cameraGlobalView) ;
			cameraGlobalView .position.y = -25;
			cameraGlobalView .position.z = 15;
			cameraGlobalView .rotation.x = 1.48; 

			//camera very hight (all circuit view)
			var camera = new THREE.OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, - 500, 10000 );
			camera.position.x = -10;
			camera.position.y = -10;
			//camera.rotation.x = -3.1415/2;

			var cameras = [cameraGlobalView,RC.camera,camera]

			requestAnimationFrame( render );
			PositionChange(car0.position);
			if(!(over)) {
				handleKeys();
			// RC.camera.up = new THREE.Vector3(1,1,0);
			// Vehicle stabilization 
				vehicle.stabilizeSkid(50) ; 
				vehicle.stabilizeTurn(1000) ;
				var oldPosition = vehicle.position.clone() ;
				vehicle.update(1.0/60) ;
				var newPosition = vehicle.position.clone() ;
				newPosition.sub(oldPosition) ;

				recordPositions[indexPosition] = newPosition;		// store each position to replay
				indexPosition++;

				// NAV
				NAV.move(newPosition.x, newPosition.y, 150,10) ;
				// car0
				car0.position.set(NAV.x, NAV.y, NAV.z) ;
				// Updates the vehicle
				vehicle.position.x = NAV.x ;
				vehicle.position.y = NAV.Y ;
			}
			else if(over == true && !(replayOver)) {		// race over, replay
				if(indexPosition < recordPositions.length) {
					replay(indexPosition, indexRotation);
					indexPosition++;
					indexRotation++;
				}
				else { 					// end of replay
					replayOver = true;
					beatThePhantom = true;		// activation "Beat The Phantom" mode
				}
			}

			// Updates car1
			car1.matrixAutoUpdate = false;		
			car1.matrix.copy(NAV.localMatrix(CARx,CARy));
			// Updates car2
			if(!(over)) { 
				car2.rotation.z = vehicle.angles.z-Math.PI/2.0; 
				recordRotations[indexRotation] = vehicle.angles.z-Math.PI/2.0; 
				indexRotation++;
			}
			// Rendering
			RC.renderer.render(RC.scene, cameras[currentCamera]);
		//}
	}

	function renderPhantom() {
		if(!(startedPhantomMode)) {		// initialisations
			initPhantomMode();
		}
		//Camera in circuit
		RC.camera.position.set(-250,250,30);//Camera 1

		RC.camera.up = new THREE.Vector3(0,0,1);
		RC.camera.lookAt(car0.position);

		//camera in car

		var cameraGlobalView = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.01, 5000 );
		car3.add(cameraGlobalView) ;
		cameraGlobalView .position.y = -25;
		cameraGlobalView .position.z = 15;
		cameraGlobalView .rotation.x = 1.48; 

		//camera very hight (all circuit view)
		var camera = new THREE.OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, - 500, 10000 );
		camera.position.x = -10;
		camera.position.y = -10;
		//camera.rotation.x = -3.1415/2;

		var cameras = [cameraGlobalView,RC.camera,camera]

		requestAnimationFrame( render );
		PositionChange(car0.position);
		//if(!(over)) {
			handleKeys();
		// RC.camera.up = new THREE.Vector3(1,1,0);
		// Vehicle stabilization 
			vehicle.stabilizeSkid(50) ; 
			vehicle.stabilizeTurn(1000) ;
			var oldPosition = vehicle.position.clone() ;
			vehicle.update(1.0/60) ;
			var newPosition = vehicle.position.clone() ;
			newPosition.sub(oldPosition) ;

			/*recordPositions[indexPosition] = newPosition;		// store each position to replay
			indexPosition++;*/

			// NAV
			NAV.move(newPosition.x, newPosition.y, 150,10) ;
			// car0
			car0.position.set(NAV.x, NAV.y, NAV.z) ;
			// Updates the vehicle
			vehicle.position.x = NAV.x ;
			vehicle.position.y = NAV.Y ;
		//}
		//else if(over == true && !(replayOver)) {		// race over, replay
			if(indexPosition < recordPositions.length) {
				replayPhantom(indexPosition, indexRotation);
				indexPosition++;
				indexRotation++;
			}
			/*else { 					// end of replay
				replayOver = true;
				beatThePhantom = true;		// activation "Beat The Phantom" mode
			}*/
		//}

		// Updates car1
		car1.matrixAutoUpdate = false;		
		car1.matrix.copy(NAV.localMatrix(CARx,CARy));
		// Updates car2
		if(!(over)) { 
			car2.rotation.z = vehicle.angles.z-Math.PI/2.0; 
			/*recordRotations[indexRotation] = vehicle.angles.z-Math.PI/2.0; 
			indexRotation++;*/
		}
		// Rendering
		RC.renderer.render(RC.scene, cameras[currentCamera]);
	};

	if(!(beatThePhantom) && !(replayOver)) {render();} 
	//else {renderPhantom();}
}
